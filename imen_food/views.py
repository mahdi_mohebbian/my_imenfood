import logging
from datetime import timedelta
from time import sleep

from django.contrib.auth import hashers
from django.http import HttpResponse
from django.shortcuts import render
from django.template import loader
from django.views.generic import ListView
from rest_framework.authtoken.models import Token
from rest_framework import parsers, permissions
from rest_framework.views import APIView
from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework import status
from imen_food_api import settings
from . import models
from . import serializers

from django.utils.timezone import localtime, now



import requests
import json


from . import jalali


def Write_to_log_file(content):
    f = open('logfile.txt', 'w+')
    f.write(content)
    f.write("")
    f.close()



# class designed to to perform notification sending to all
class My_class():
    def SendNotificationToAll(self,authorization_key, content, heading, app_id):
        header = {"Content-Type": "application/json; charset=utf-8",
                  "Authorization": "Basic " + authorization_key}

        payload = {"app_id": app_id,
                   "included_segments": ["All"],
                   "contents": {"en": content},
                   "headings": {"en": heading}}

        req = requests.post("https://onesignal.com/api/v1/notifications", headers=header, data=json.dumps(payload))

        print(req.status_code, req.reason)
        print("sent to all")




# the viewset classes that contains abstract and detailed info about each branch
class RestaurantViewSet(viewsets.ModelViewSet):
    serializer_class = serializers.RestaurantSerializer
    queryset = models.Restaurant.objects.all()





class BranchViewSet_abstract(viewsets.ModelViewSet):
    serializer_class = serializers.BranchSerializer_abstract
    queryset = models.Branch.objects.all()



class BranchViewSet_detail(viewsets.ModelViewSet):
    serializer_class = serializers.BranchSerializer_detail
    queryset = models.Branch.objects.all()


# APIView designed to retrieve user's branch data - in restaurant detail
class BranchAPIView_detail_user(APIView):
    def get(self,request):
        branch = models.UserProfile.objects.get(id=request.user.id).branch
        serializer = serializers.BranchSerializer_detail(branch)
        return Response(serializer.data)
    def post(self,request):
        pass
# APIView designed to retrieve user's branch data - in abstract info about restaurant
class BranchAPIView_abstract_user(APIView):
    def get(self,request):
        branch = models.UserProfile.objects.get(id=request.user.id).branch
        serializer = serializers.BranchSerializer_abstract(branch)
        return Response(serializer.data)
    def post(self,request):
        pass



# Viewsets designed for equipments and Restaurant(not branches)
class EquipmentViewSet(viewsets.ModelViewSet):
    serializer_class = serializers.EquipmentSerializer
    queryset = models.Equipment.objects.all()



class RestaurantsEquipmentViewSet(viewsets.ModelViewSet):
    serializer_class = serializers.RestaurantsEquipmentSerializer_user_detail
    queryset = models.RestaurantsEquipment.objects.all()




#APIView for user's branch equipment list
class RestaurantsEquipmentAPIView(APIView):

    def get(self,request):
        user = models.UserProfile.objects.get(id=request.user.id)
        '''
        param = request.query_params.get('param', None) =============== sample of query params (url+"?param=some_string")
        print("+++++++++++++++++"+str(request.GET.get('rest'))) =============== sample for query parameters
        '''

        equips = models.RestaurantsEquipment.objects.filter(restaurant=user.branch)

        serializer = serializers.RestaurantsEquipmentSerializer_user_detail(equips,many=True)

        return Response(serializer.data)

    def post(self,request):
        serializer = serializers.RestaurantsEquipmentSerializer_user_abstract(data=request.data,context={'request': self.request})
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data,status=status.HTTP_201_CREATED)
        return Response(serializer.errors,status=status.HTTP_400_BAD_REQUEST)


class RestaurantEquipmentDetail_put_deleteAPIView(APIView):#NEW

    def delete(self,request,pk):
        try :
            models.RestaurantsEquipment.objects.get(id=pk).delete()

            return Response({'message': 'tools have been modified'}, status=status.HTTP_204_NO_CONTENT)
        except:
            return Response({'message': 'something went wrong'}, status=status.HTTP_400_BAD_REQUEST)











# viewsets for providers
class ProviderViewSet(viewsets.ModelViewSet):
    serializer_class = serializers.ProviderSerialzier
    queryset = models.Provider.objects.all()





# viewsets for receiving all the users data
class UserProfileViewSet(viewsets.ModelViewSet):
    serializer_class = serializers.UserProfileSerializer
    queryset = models.UserProfile.objects.all()


# APIView for receiving current user info
class MyUserProfileAPIVIew(APIView):
    def get(self,request):
        user = models.UserProfile.objects.get(id=request.user.id)
        serializer = serializers.UserProfileSerializer(user)
        final_data = serializer.data



        branch = user.branch

        morning_answers = models.Answers.objects.filter(Q_id__section="morning",date=localtime().date(),branch=branch)



        during_answers = models.Answers.objects.filter(Q_id__section="during",date=localtime().date(),branch=branch)


        closing_answers = models.Answers.objects.filter(Q_id__section="closing",date=localtime().date(),branch=branch)



        if len(morning_answers) != 0:
            morning = True
        else:
            morning = False

        if len(during_answers) != 0:
            during = True
        else:
            during = False

        if len(closing_answers) != 0:
            closing = True
        else:
            closing = False


        answers_dict = {"Morning":morning,
                        "During":during,
                        "Closing":closing
                        }
        final_data.update(answers_dict)

        hours = models.WorkingHours.objects.all()[0]
        hours_data = {"morning": {"start": hours.morning_start, "stop": hours.morning_end},
                      "during": {"start": hours.during_start, "stop": hours.during_end},
                      "closing": {"start": hours.closing_start, "stop": hours.closing_end}}
        hours_dict = {"Hours":hours_data}
        final_data.update(hours_dict)
        return Response(final_data,status=status.HTTP_200_OK)

# viewsets for providers and products
class Product_ProviderViewSet(viewsets.ModelViewSet):
    serializer_class = serializers.Provider_ProductSerializer
    queryset = models.Product_Provider.objects.all()



# viewset for pengind admin panel  list
class RestaurantPendingViewSet(viewsets.ModelViewSet):
    serializer_class = serializers.AdminPendingSerializer
    queryset = models.AdminPending.objects.all()

# return the products of the specified provider
class Provider_productResponse(APIView):
    def post(self,request):
        my_id = request.data['id']
        product_provider = models.Product_Provider.objects.filter(provider=models.Provider.objects.get(id=int(my_id)))

        lst = list()
        for item in product_provider:
            lst.append(serializers.ProductSerializer(item.product).data)
        products = {'products':lst}


        return Response(products)




# APIView for receiving questions using Query Params
class QuestionAPIView(APIView):

    def get(self,request):
        section = request.query_params.get('section',None)
        if section == None:
            questions = models.Question.objects.all()
            serializer = serializers.QuestionSerializer(questions,many=True)
            return Response(serializer.data)
        else:
            questions = models.Question.objects.filter(section=section)
            serializer = serializers.QuestionSerializer(questions, many=True)
            return Response(serializer.data)

    def post(self,request):
        pass
# APIView for question feeds
class QuestionFeedAPIVIew(APIView):
    def get(self,request):
        q_feeds = models.QuestionFeed.objects.all()
        serializer = serializers.QuestionFeedSerializer(q_feeds,many=True)
        return Response(serializer.data)
    def post(self,requst):
        pass
# APIView for answers
class AnswerAPIView(APIView):
    def get(self,request):
        answers = models.Answers.objects.filter(branch=models.UserProfile.objects.get(id=request.user.id).branch)
        serializer = serializers.AnswerSerializer(answers,many=True)
        return Response(serializer.data)

    def post(self,request):

        serialzier_array = serializers.AnswerSerializer(data=request.data,many=True,context={'request': self.request})
        if serialzier_array.is_valid():
            data_array = serialzier_array.validated_data

            branch = models.UserProfile.objects.get(id=request.user.id).branch

            main_section = data_array[0]['Q_id'].section
# checks if the time for answering the corresponding sectoion is over or not
            if (main_section=='morning'):
                if localtime(now()).hour < models.WorkingHours.objects.all()[0].morning_start or localtime(now()).hour > \
                        models.WorkingHours.objects.all()[0].morning_end:
                    return Response({'message':'Time for answering these questions is over ! '},status=status.HTTP_422_UNPROCESSABLE_ENTITY)
            elif(main_section=='during'):
                if localtime(now()).hour < models.WorkingHours.objects.all()[0].during_start or localtime(now()).hour > models.WorkingHours.objects.all()[0].during_end:# create a static class for this information
                    return Response({'message':'Time for answering these questions is over ! '},status=status.HTTP_422_UNPROCESSABLE_ENTITY)
            else:
                if localtime(now()).hour < models.WorkingHours.objects.all()[0].closing_start or localtime(now()).hour > \
                        models.WorkingHours.objects.all()[0].closing_end:
                    return Response({'message':'Time for answering these questions is over ! '},status=status.HTTP_422_UNPROCESSABLE_ENTITY)




            todays_ans = models.Answers.objects.filter(date=localtime().date(),Q_id__section=main_section,branch=branch)

            if len(todays_ans)!=0:
                return Response({'message':'you already answered to these questions'},status=status.HTTP_400_BAD_REQUEST)

            questions = models.Question.objects.filter(section=main_section)

            num_of_questions=0

            for data in data_array:
                for q in questions :
                    if q.id == data['Q_id'].id:
                        num_of_questions = num_of_questions+1

# check if all of the questions has been answered
            if (len(questions)) != num_of_questions :
                return Response({'message':'you should send all of the answers'})


            for data in data_array:

                ans = models.Answers(
                    answer = data['answer'],
                    type = data['type'],
                    desc = data['desc'],
                    Q_id = data['Q_id'],
                    branch = branch

                )
                ans.save()

            return Response({'message':'Answers created'})
        else:
            return Response({'message':'some thing went wrong'},status=status.HTTP_400_BAD_REQUEST)








# APIView for admin pending list
class AdminPendingAPIView(APIView):
    permission_classes = (permissions.AllowAny,)
    def get(self,request):
        pendings=models.AdminPending.objects.all()
        serializer = serializers.AdminPendingSerializer(pendings,many=True)
        return Response(serializer.data,status=status.HTTP_200_OK)

    def post(self,request):
        serializer = serializers.AdminPendingSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data,status=status.HTTP_201_CREATED)
        return Response(serializer.errors,status=status.HTTP_400_BAD_REQUEST)


# setting permissions to allow any to let any one try for signing up
class LoginAPIView(APIView):
    permission_classes = (permissions.AllowAny,)
    def get(self,request):
        return Response({'message':'get'})
    def post(self, request):
        password = request.data['password']
        my_user_name = request.data['user_name']

        try:
            user = models.UserProfile.objects.get(email=my_user_name)
        except models.UserProfile.DoesNotExist:
            user=None
# this means the user does exist check for password
        if user != None :
            print("----------------- every thing works so far"+hashers.check_password(password=password , encoded=user.password).__str__())
            if hashers.check_password(password=password , encoded=user.password):
# the user exist and password is right
                try :
                    users_token =Token.objects.get(user=user)
                    users_token.delete()
                except:
                    pass
                token = Token.objects.create(user=user)
                return Response({'Token': token.__str__()},status=status.HTTP_200_OK)
            else:
                return Response({'message':'wrong password'},status=status.HTTP_401_UNAUTHORIZED)
        else: # user does not exist
            return Response({'message':'the user does not exist'},status=status.HTTP_404_NOT_FOUND)

class Login_branch_admin_APIView(APIView):

    permission_classes = (permissions.AllowAny,)

    def get(self, request):
        return Response({'message': 'get'})

    def post(self, request):
        password = request.data['password']
        my_user_name = request.data['user_name']

        try:
            user = models.UserProfile.objects.get(email=my_user_name)
        except models.UserProfile.DoesNotExist:
            user = None
            # this means the user does exist check for password
        if user != None:
            print("----------------- every thing works so far" + hashers.check_password(password=password,
                                                                                        encoded=user.password).__str__())
            if hashers.check_password(password=password, encoded=user.password):
                # the user exist and password is right
                try:
                    users_token = Token.objects.get(user=user)
                    users_token.delete()
                except:
                    pass
                if user.admin_panel_access == True:
                    token = Token.objects.create(user=user)
                    return Response({'Token': token.__str__()}, status=status.HTTP_200_OK)
                else :
                    return Response({'message': 'user does not have admin panel permission'}, status=status.HTTP_401_UNAUTHORIZED)
            else:
                return Response({'message': 'wrong password'}, status=status.HTTP_401_UNAUTHORIZED)
        else:  # user does not exist
            return Response({'message': 'the user does not exist'}, status=status.HTTP_404_NOT_FOUND)


# FAQ APIView
class FAQAPIView(APIView):
    def get(self,request):
        FAQs = models.FAQ.objects.all()
        serializer = serializers.FAQSerializer(FAQs,many=True)
        return Response(serializer.data,status=status.HTTP_200_OK)


class ReceiptSendAPIVIew(APIView):

    def get(self,request):
        pass

# posts all the needed data along with the uploaded image path
    def post(self,request):
        data = request.data
        serializer = serializers.ReceiptSendSerialzier(data=data)
        if serializer.is_valid():

            my_receipt = models.Receipt_cart(
                image=serializer.validated_data['image'],
                user = models.UserProfile.objects.get(id=request.user.id)
            )
            my_receipt.save()

            for factor in serializer.validated_data['product_provider_data']:

                my_factor = models.Factor(
                    provider = models.Provider.objects.get(id=int(factor['provider'])),
                    product = models.Product.objects.get(id=int(factor['product'])),
                    receipt = my_receipt
                )
                my_factor.save()
            return Response({'message':'receipt been saved'},status = status.HTTP_201_CREATED)
        return Response(serializer.errors,status=status.HTTP_400_BAD_REQUEST)


# a hand made APIView for sending away working hours
class WorkingHourPIView(APIView):
    def get(self,request):
        hours = models.WorkingHours.objects.all()[0]
        return Response({"morning":{"start":hours.morning_start,"stop":hours.morning_end},
                        "during":{"start":hours.during_start,"stop":hours.during_end},
                        "closing":{"start":hours.closing_start,"stop":hours.closing_end}})


# APIView for uploading image on server.
class ImageuploadAPIView(APIView):

    def post(self,request):
        Write_to_log_file("it is using the imageupload API")
        try:
            Write_to_log_file("it is using the imageupload API")
            file = request.data['file']
            my_image = models.UploadedImages(image=file)
            my_image.save()
            path = my_image.__str__()
            Write_to_log_file(path)
            return Response({'image_path':path},status = status.HTTP_201_CREATED)
        except Exception as inst:
            Write_to_log_file(str(inst))
            return Response({'message': 'failed to upload the image'}, status=status.HTTP_400_BAD_REQUEST)



class Support_and_Rules_LinkAPIView(APIView):
    permission_classes = (permissions.AllowAny,)

    def get(self,request):

        links = models.DynamicLinks.objects.all()[0]
        return Response({"support":links.support_link,"rules":links.rules_link})

'''
from here there are views designed for panels
'''




#APIView receiving player id from the user's phone to set for notification system
class PlayerIDAPIView(APIView):
    def post(self,request):
        player_id = request.data['player_id']
        user = models.UserProfile.objects.get(id = request.user.id)
        user.OneSignal_id = player_id
        user.save()
        return Response({"message":"one signal id has been sent"},status=status.HTTP_202_ACCEPTED)

# sending away the notifications
class NotificationsAPIView(APIView):
    def get(self,request):
        Write_to_log_file("you are in the links area ! ")
        notifs = models.Notifications.objects.filter(user_id=request.user)
        serializer = serializers.NotificationsSerializer(notifs,many=True)
        return Response(serializer.data,status=status.HTTP_200_OK)


# APIView for sending all the users same notification using one signal
class NotificationSendToAll(APIView):
    permission_classes = (permissions.IsAdminUser,)
    def post(self,request):
        try:
            content = request.data['content']
            title = request.data['title']
            mc = My_class()
            mc.SendNotificationToAll("ZTY0ZTNmNjMtM2MwYS00ZTQ0LWJmODUtZGIwZTYyMWNjM2Ew",
                                  content, title,
                                  "b51bb857-6a46-4119-a88c-ff6dac3ed079")

            for user in models.UserProfile.objects.all():
                notif = models.Notifications(content=content,
                                             title=title,
                                             is_all=True,
                                             user_id=user
                                             )
                notif.save()
            return Response({'message':'message sent to all!'},status=status.HTTP_200_OK)
        except:
            return Response({'message': 'execution fail'}, status=status.HTTP_400_BAD_REQUEST)









class MyBranchUsers_APIView(APIView):
    def get(self,request):
        try:
            branch = models.UserProfile.objects.get(id=request.user.id).branch
            my_users = models.UserProfile.objects.filter(branch=branch)
            serializer = serializers.UserProfileSerializer(my_users,many=True)
            return Response(serializer.data,status=status.HTTP_200_OK)
        except:
            return Response({'message':'users not found'},status=status.HTTP_400_BAD_REQUEST)







class RegisterAPIView(APIView):# APIView for registering in admin panel
    def post(self,request):
        email = request.data['email']
        phone = request.data['phone_number']


        users_e = models.UserProfile.objects.filter(email=email)



        users_ph = models.UserProfile.objects.filter(phone_number=phone)



        if len(users_e)==0 and len(users_ph)==0 :
            user = models.UserProfile.objects.get(id = request.user.id)
            branch = user.branch
            my_password = hashers.make_password(request.data['password'])
            new_user = models.UserProfile(name=request.data['name'],
                                                      phone_number=request.data['phone_number'],
                                                      email=request.data['email'],
                                                      password=my_password,
                                                      admin_panel_access=request.data['admin_panel_access'],
                                                      is_active=True,
                                                      authorization_level=0,
                                                      profile_picture=request.data['profile_picture'],
                                                      OneSignal_id="no id yet",
                                                      branch=branch
                                                )
            new_user.save()
            Token.objects.create(user=new_user)
            return Response({"message":"you've been registered"},status=status.HTTP_201_CREATED)
        else :
            return Response({"message": "phone number or email address has been used"}, status=status.HTTP_406_NOT_ACCEPTABLE)

        






class ReInvestigateAPIView(APIView):
    def get(self,request):
        requests = models.ReInvestigateReuqeustList.objects.all()
        serializer = serializers.ReInvestigateSerializer(requests,many=True)
        return Response(serializer.data,status=status.HTTP_200_OK)

    def post(self,request):
        branch = models.UserProfile.objects.get(id = request.user.id).branch

        new_request = models.ReInvestigateReuqeustList.objects.create(branch=branch,
                                                                      message=request.data['message'])
        new_request.save()
        return Response({"message":"Reuqest has been sent"})


class FactorAdminPanelPopulationAPIView(APIView):
    def get(self,request):

        receipts = models.Receipt_cart.objects.all()

        results = dict()




        for receipt in receipts :
            receipt_detail_dict = dict()
            factor_dict = dict()
            factor_list = list()
            factors = models.Factor.objects.filter(receipt=receipt)

            receipt_detail_dict["date"] = receipt.date.__str__()
            receipt_detail_dict["user"] = serializers.UserProfileSerializer(receipt.user).data
            receipt_detail_dict["image"] = receipt.image.url
            for factor in factors:
                factor_dict["provider"]=factor.provider.__str__()
                factor_dict["product"] = factor.product.__str__()
                factor_list.append(factor_dict)

            receipt_detail_dict["provider_product"]=factor_list

            results[receipt.id.__str__()]= receipt_detail_dict


        return Response(results, status=status.HTTP_200_OK)






class UserProfileDetail_APIView(APIView):#NEW
    def put(self,request,pk):
        try :
            user = models.UserProfile.objects.get(id=pk)
            user.name = request.data['name']
            user.email = request.data['email']
            user.admin_panel_access = request.data['admin_panel_access']
            user.phone_number = request.data['phone_number']
            user.profile_picture = request.data['profile_picture']
            if request.data['profile_picture']=="":
                user.profile_picture = "images/null.jpg"
            user.save()
            return Response({'message':'user completely modified'},status=status.HTTP_200_OK)
        except:
            return Response({'message':'something went wrong'},status=status.HTTP_400_BAD_REQUEST)
    def delete(self,request,pk):
        try:
            user = models.UserProfile.objects.get(id=pk)
            user.delete()
            return Response({'message':'user has been deleted'},status=status.HTTP_204_NO_CONTENT)
        except:
            return Response({'message': 'something went wrong'}, status=status.HTTP_400_BAD_REQUEST)



class LogoutAPIVIew(APIView):
    def get(self,request):
        try:
            user = models.UserProfile.objects.get(id=request.user.id)
            user.OneSignal_id=""
            user.save()
            return Response({'message':'old player id has been removed'},status=status.HTTP_204_NO_CONTENT)
        except:
            return Response({'message':'user not found'},status=status.HTTP_404_NOT_FOUND)

import datetime
class ActivitiesAPIView(APIView):
    def get(self,request):
        start_date_string = str(request.query_params.get('start_date', None))
        end_date_string = str(request.query_params.get('end_date', None))
        start_date = datetime.date(year=int(start_date_string.split('-')[0]),month=int(start_date_string.split('-')[1]),
                                   day=int(start_date_string.split('-')[2]))

        end_date = datetime.date(year=int(end_date_string.split('-')[0]),
                                   month=int(end_date_string.split('-')[1]),
                                   day=int(end_date_string.split('-')[2]))  #

        my_start_date = start_date# jalali.Persian(start_date).gregorian_datetime()
        my_end_date = end_date#jalali.Persian(end_date).gregorian_datetime()
        branch = models.UserProfile.objects.get(id=request.user.id).branch
        days_count = (my_end_date - my_start_date).days+1
        dates_list = list()
        final_result_dict = dict()


        for single_date in (my_start_date + timedelta(n) for n in range(days_count)):
            dates_list.append(single_date)

        answers_in_dates = models.Answers.objects.filter(branch=branch,date__range=(my_start_date,my_end_date))

        final_list = list()

        for my_date in dates_list:


            morning_answers = answers_in_dates.filter(Q_id__section="morning",date=my_date)
            during_answers = answers_in_dates.filter(Q_id__section="during",date=my_date)
            closing_answers = answers_in_dates.filter(Q_id__section="closing",date=my_date)

            if len(morning_answers) != 0:
                morning = True
            else:
                morning = False

            if len(during_answers) != 0:
                during = True
            else:
                during = False

            if len(closing_answers) != 0:
                closing = True
            else:
                closing = False

            answers_dict = {"Date":my_date,
                            "Morning": morning,
                            "During": during,
                            "Closing": closing
                            }
            final_list.append(answers_dict)
        final_result_dict["Calender_data"]=final_list

        return Response(final_result_dict,status=status.HTTP_200_OK)


class register_restaurant_manager_APIView(APIView):
    def post(self,request):
        name = request.data["name"]
        phone_number = request.data["phone_number"]
        email = request.data["email"]
        branch = request.data["branch"]
        password = request.data["password"]

        user = models.UserProfile(name=name,
                                  phone_number=phone_number,
                                  email=email,
                                  branch=models.Branch.objects.get(id=int(branch)),
                                  admin_panel_access=True,
                                  OneSignal_id='no id yet',
                                  authorization_level=1,
                                  is_active=True,
                                  password=hashers.make_password(password)
                                  )
        user.save()
        return Response({"message":"restaurant manager has been registered ! "},status=status.HTTP_201_CREATED)