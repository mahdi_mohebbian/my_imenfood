# -*- coding: utf-8 -*-
# Generated by Django 1.11.3 on 2017-12-24 09:47
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('imen_food', '0026_auto_20171224_1117'),
    ]

    operations = [
        migrations.CreateModel(
            name='NearHours',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('morning_hour', models.IntegerField()),
                ('morning_minute', models.IntegerField()),
                ('during_hour', models.IntegerField()),
                ('during_minute', models.IntegerField()),
                ('closing_hour', models.IntegerField()),
                ('closing_minute', models.IntegerField()),
            ],
        ),
    ]
