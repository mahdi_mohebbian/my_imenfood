# -*- coding: utf-8 -*-
# Generated by Django 1.11.3 on 2017-12-31 10:57
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('imen_food', '0033_userprofile_admin_panel_access'),
    ]

    operations = [
        migrations.CreateModel(
            name='ReInvestigateReuqeustList',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date', models.DateField(auto_now=True)),
                ('branch', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='imen_food.Branch')),
            ],
            options={
                'ordering': ['-date'],
            },
        ),
    ]
