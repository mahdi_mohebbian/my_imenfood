from rest_framework import serializers
from . import models


#serializer class for Restaurant model
class RestaurantSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Restaurant
        fields = ('id','name','logo')



#serializer class for Branch model - where the restaurant field is populated with only "id"
class BranchSerializer_abstract(serializers.ModelSerializer):
    class Meta:
        model = models.Branch
        fields = ('id','branch','add','email','phone_number','industrial_code','reference_number','restaurant')


#serializer class for Branch model - where the restaurant field is populated with detailed information
class BranchSerializer_detail(serializers.ModelSerializer):
    restaurant = RestaurantSerializer()
    class Meta:
        model = models.Branch
        fields = ('id','branch','add','email','phone_number','industrial_code','reference_number','restaurant')

		

#serializer class for Equipment model
class EquipmentSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Equipment
        fields = '__all__'


#serializer class for RestaurantsEquipment model
class RestaurantsEquipmentSerializer_user_abstract(serializers.ModelSerializer):

    class Meta:
        model = models.RestaurantsEquipment
        fields='__all__'
        extra_kwargs = {'restaurant': {'read_only': True}}

    def create(self, validated_data):
# pass the user's branch as the "branch" field automaticly
        branch = models.Branch.objects.get(id = self.context['request'].user.branch.id )
        rest_equip = models.RestaurantsEquipment(restaurant =branch,
                                                equipment=validated_data['equipment'])

        rest_equip.save()
        return rest_equip
'''
        #rest_equip = models.RestaurantsEquipment.objects.get(
         #                                                   restaurant=models.Branch.objects.get(id = self.context['request'].user.branch.id ),
          #                                                   equipment=models.Equipment.objects.get(id = validated_data['equipment'].id)
           #                                                  )
'''

#serializer class for RestaurantsEquipment model in details
class RestaurantsEquipmentSerializer_user_detail(serializers.ModelSerializer):
    equipment = EquipmentSerializer()

    class Meta:
        model = models.RestaurantsEquipment
        fields='__all__'
        extra_kwargs = {'restaurant': {'read_only': True}}

    def create(self, validated_data):
        branch = models.Branch.objects.get(id = self.context['request'].user.branch.id )
        rest_equip = models.RestaurantsEquipment(restaurant =branch,
                                                equipment=validated_data['equipment'])

        rest_equip.save()
        return rest_equip




#serializer class for Provider model
class ProviderSerialzier(serializers.ModelSerializer):


    class Meta:
        model = models.Provider
        fields = ('id','name')
        """,'some_info')
    #def get_some_info(self,obj):        ---------- baraie ezafe kardane field haie jadid bedune niaz be model
        #return 15       ---------- baraie ezafe kardane field haie jadid bedune niaz be model"""
        """
            #some_info = serializers.SerializerMethodField()    ---------- baraie ezafe kardane field haie jadid bedune niaz be model
        """

#serializer class for Product model
class ProductSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Product
        fields = ('id','name')





#serializer class for Provider_Product model
class Provider_ProductSerializer(serializers.ModelSerializer):
    provider = ProviderSerialzier()
    product = ProductSerializer()

    class Meta:
        model = models.Product_Provider
        fields = ('id','provider','product')



#serializer class for UserProfile model
class UserProfileSerializer(serializers.ModelSerializer):
    branch = BranchSerializer_detail()
    class Meta:
        model = models.UserProfile
        fields = ('id', 'email', 'name', 'phone_number', 'password','admin_panel_access','profile_picture','branch')
        extra_kwargs = {'password': {'write_only': True}
                        }






#serializer class for Questions model
class QuestionSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Question
        fields = '__all__'

#serializer class for QuestionFeed model
class QuestionFeedSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.QuestionFeed
        fields = '__all__'

#serializer class for Answers model
class AnswerSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Answers
        fields = '__all__'
        extra_kwargs = {'branch': {'read_only': True}}

    


#serializer class for Pending model
class AdminPendingSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.AdminPending
        fields = '__all__'
    ''' logo = Base64ImageField(max_length=None, use_url=True)'''


#serializer class for FAQ model
class FAQSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.FAQ
        fields = '__all__'




class Provider_Product_TemplateSerializer(serializers.Serializer): 
    product = serializers.IntegerField()
    provider = serializers.IntegerField()

#serializer class for Receipt model
class ReceiptSendSerialzier(serializers.Serializer):
    image = serializers.CharField(max_length=60)
    product_provider_data = Provider_Product_TemplateSerializer(many=True)

#serializer class for image upload section model
class UploadImage(serializers.ModelSerializer):
    class Meta:
        model = models.UploadedImages
        fields = ('id','image')

#serializer class for notifications model
class NotificationsSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Notifications
        fields = ('id','content','title','date','time')

#serializer class for signup page ... which connects to  UserProfile
class RegisterSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.UserProfile
        fields = ('id', 'email', 'name', 'phone_number', 'password', 'authorization_level', 'profile_picture')
        extra_kwargs = {'password': {'write_only': True}}

#serializer class for ReInvestigate model
class ReInvestigateSerializer(serializers.ModelSerializer):
    branch = BranchSerializer_detail()
    class Meta:
        model= models.ReInvestigateReuqeustList
        fields = ('id','date','branch','message')
