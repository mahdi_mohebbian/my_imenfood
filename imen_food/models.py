import os
from django.contrib.auth.models import User
from django.db import models
from django.contrib.auth.models import AbstractBaseUser,BaseUserManager,PermissionsMixin

from django.dispatch import receiver

# this model is designed for pending list in admin panel
class AdminPending(models.Model):
    manager_name = models.CharField(max_length=50)
    restaurant_name = models.CharField(max_length=20)
    add = models.TextField()
    email = models.EmailField(unique=True)
    phone_number = models.BigIntegerField()

    def __str__(self):
        return self.id.__str__()+": "+"pending : "+ self.manager_name+" - "+self.restaurant_name
'''
    logo = models.ImageField(default='images/null.jpg',upload_to='images/')
'''

# restaurant of branches model
class Restaurant(models.Model):
    name = models.CharField(max_length=20,unique=True)
    logo = models.ImageField(default='images/null.jpg',upload_to='images/')

    def __str__(self):
        return self.id.__str__()+": "+self.name

# branch model that contains the detailed information about each branch
class Branch(models.Model):
    branch = models.CharField(max_length=50)
    add = models.TextField()
    email = models.EmailField(unique=True)
    phone_number = models.BigIntegerField()
    industrial_code = models.BigIntegerField()
    reference_number = models.BigIntegerField()
    restaurant = models.ForeignKey(Restaurant)
    def __str__(self):
        return self.id.__str__()+": "+self.restaurant.__str__() + " - " + self.branch


# i overrided this class in order to customize the user object behaviour
class UserProfileManager(BaseUserManager):

    def create_user(self,email,name,phone_number,password=None):

        email = self.normalize_email(email)
        user = self.model(email=email,name=name,phone_number=phone_number,password=password)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, email, name, phone_number, password=None):
        user = self.create_user(email=email,name=name,phone_number=phone_number,password=password)
        user.is_staff = True
        user.is_superuser = True
        user.save(using=self._db)

# user profile model ... overriden from django.auth.User
class UserProfile(AbstractBaseUser,PermissionsMixin):
    name = models.CharField(max_length=100)
    phone_number = models.BigIntegerField(unique=True)
    email = models.EmailField(unique=True)
    created_on = models.DateField(auto_now=True)
    is_active = models.BooleanField(default=True)
    is_staff = models.BooleanField(default=False)
    authorization_level = models.IntegerField(default=0) 

    branch = models.ForeignKey(Branch, null=True)
    profile_picture = models.ImageField(default='images/null.jpg',upload_to='images/')
    OneSignal_id = models.CharField(max_length=150)
    admin_panel_access = models.BooleanField(default=False)
    objects = UserProfileManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['phone_number', 'name']

    def get_short_name(self):
        return self.name

    def get_full_name(self):
        return self.name

    def __str__(self):
        return self.id.__str__()+" :"+self.name
'''
	# 0 for ordinary users - 1 for restaurant manager - 2 for system admin
'''
# equipment model
class Equipment(models.Model):
    name = models.CharField(max_length=15)
    advice = models.TextField(default='no particular advice !')
    def __str__(self):
        return self.id.__str__()+" : "+self.name

# a joint class which connects the equipments and branches together
class RestaurantsEquipment(models.Model):
    restaurant = models.ForeignKey(Branch)
    equipment = models.ForeignKey(Equipment)
    def __str__(self):
        return self.id.__str__()+": "+self.restaurant.__str__() + " - " + self.equipment.__str__()


# providers company model
class Provider(models.Model):
    name = models.CharField(max_length=15)
    def __str__(self):
        return self.name
    class Meta:
        ordering = ['name']

# products company model
class Product(models.Model):
    name = models.CharField(max_length=15)
    def __str__(self):
        return self.id.__str__()+": "+self.name
    class Meta:
        ordering = ['name']

# a joint class which connects the providers and products together
class Product_Provider(models.Model):
    provider = models.ForeignKey(Provider)
    product = models.ForeignKey(Product)

    class Meta:
        unique_together=('provider','product')
    def __str__(self):
        return self.id.__str__()+": "+self.product.__str__()+" "+self.provider.__str__()


# this model has been designed to store image and user data of each uploaded receipt
class Receipt_cart(models.Model):
    image = models.ImageField(default='images/null.jpg',upload_to='images/')
    user = models.ForeignKey(UserProfile)
    date = models.DateField(auto_now=True)
    class Meta:
        ordering = ['-date']
    def __str__(self):
        return "Receipt :"+self.user.__str__()+" - "+self.date.__str__()

# factor model has been designed to store providers and products of each uploaded receip
class Factor(models.Model):
    provider = models.ForeignKey(Provider)
    product = models.ForeignKey(Product)
    receipt = models.ForeignKey(Receipt_cart) # which has a foreignkey to the Receipt_cart model
    def __str__(self):
        return self.id.__str__()+": "+self.provider.__str__()+" - "+self.product.__str__()+" - "+self.receipt.__str__()

# Questions Model
class Question(models.Model):
    text = models.TextField()
    section = models.TextField(max_length=20)
    class Meta:
        ordering = ['-section']
    def __str__(self):
        return "id:"+self.id.__str__()+" - - Section : "+self.section+ "- text: "+self.text

# Questions feed Model
class QuestionFeed(models.Model):
    Q_id = models.ForeignKey(Question)
    type = models.CharField(max_length=15,null=True,blank=True)
    advice = models.TextField(null=True,blank=True)
    when = models.BooleanField()
    def __str__(self):
        return self.Q_id.__str__() + " - "+ str(self.when)


# Answers Model
class Answers(models.Model):
    Q_id = models.ForeignKey(Question)
    date = models.DateField(auto_now=True)
    branch = models.ForeignKey(Branch)
    answer = models.BooleanField()
    type = models.CharField(max_length=50,null=True,blank=True)
    desc = models.CharField(max_length=100,null=True,blank=True)
    def __str__(self):
        return self.id.__str__()+": "+"Question :"+self.Q_id.__str__()+" - "+" Branch :"+self.branch.__str__()+" - "+ self.date.__str__()

# FAQ Model
class FAQ(models.Model):
    question = models.TextField()
    answer = models.TextField()
    def __str__(self):
        return self.id.__str__()+": "+self.question

# this model is used to determine the working hours threshold
class WorkingHours(models.Model):
    morning_start = models.IntegerField()
    morning_end = models.IntegerField()
    during_start = models.IntegerField()
    during_end = models.IntegerField()
    closing_start = models.IntegerField()
    closing_end = models.IntegerField()


# this model's information shows the "close enough" time limit for sending notifications.
class NearHours(models.Model):
    morning_hour = models.IntegerField()
    morning_minute = models.IntegerField()
    during_hour = models.IntegerField()
    during_minute = models.IntegerField()
    closing_hour = models.IntegerField()
    closing_minute = models.IntegerField()


# notifications model
class Notifications(models.Model):
    content = models.TextField()
    title = models.CharField(max_length=30)
    user_id= models.ForeignKey(UserProfile,null=True)
    date = models.DateField(auto_now=True)
    is_all = models.BooleanField(default=True)
    time = models.TimeField(auto_now=True,null=True)
    def __str__(self):
        return self.id.__str__()+": "+self.title + " - "+self.is_all.__str__()+" - "+self.user_id.__str__()
    class Meta:
        ordering = ['-date','-time']

# all the uploaded image model
class UploadedImages(models.Model):
    image = models.ImageField( upload_to='images/')
    def __str__(self):
        return self.image.__str__()

# these to method is designed to delete the file from storage when it got deleted in database
@receiver(models.signals.post_delete, sender=UploadedImages)
def auto_delete_file_on_delete(sender, instance, **kwargs):


    if instance.image:
        if os.path.isfile(instance.image.path):
            os.remove(instance.image.path)

@receiver(models.signals.post_delete, sender=Receipt_cart)
def auto_delete_file_on_delete(sender, instance, **kwargs):
    """
    Deletes file from filesystem
    when corresponding `MediaFile` object is deleted.
    """
    if instance.image:
        if os.path.isfile(instance.image.path):
            os.remove(instance.image.path)


# reinvestigate model queue for restaurants
class ReInvestigateReuqeustList(models.Model):
    branch = models.ForeignKey(Branch)
    date = models.DateField(auto_now=True)
    message = models.TextField(default="")
    class Meta:
        ordering = ['-date']


class DynamicLinks(models.Model):
    rules_link = models.URLField(blank=True,null=True)
    support_link = models.URLField(blank=True,null=True)

    def __str__(self):
        return "rules : "+self.rules_link.__str__() + "- support : "+ self.support_link.__str__()