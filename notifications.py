import os
import django
import datetime

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "imen_food_api.settings")
django.setup()





from apscheduler.schedulers.blocking import BlockingScheduler
from imen_food import models
from django.db import models as model_class
import requests
import json


# this function is designed to send notifications to specific users
def SendNotification_to_userIDs(id_list,authorization_key,content,heading, app_id):


    header = {"Content-Type": "application/json; charset=utf-8",
              "Authorization": "Basic "+authorization_key}

    payload = {"app_id": app_id,
               "include_player_ids": id_list,
               "contents": {"en": content},
               "headings": {"en": heading}}

    req = requests.post("https://onesignal.com/api/v1/notifications", headers=header, data=json.dumps(payload))

    print(req.status_code, req.reason)
# this function is designed to send notifications to all users
def SendNotificationToAll(authorization_key,content,heading,app_id):
    header = {"Content-Type": "application/json; charset=utf-8",
              "Authorization": "Basic "+authorization_key}

    payload = {"app_id": app_id,
               "included_segments": ["All"],
               "contents": {"en": content},
               "headings": {"en": heading}}

    req = requests.post("https://onesignal.com/api/v1/notifications", headers=header, data=json.dumps(payload))

    print(req.status_code, req.reason)
    print("sent to all")








def morning_near_over_notif():
    # morning section
    if datetime.time.hour == models.NearHours.objects.all()[0].morning_hour and datetime.time.minute == models.NearHours.objects.all()[0].morning_minute:
        rest_list_id = set()
        for ans in models.Answers.objects.all():
            if datetime.date.today() == ans.date and ans.Q_id.section == "morning":
                pass
            else:
                rest_list_id.add(ans.branch.id)

        print(rest_list_id)
        user_list_id = list()
        for user in models.UserProfile.objects.all():
            if rest_list_id.__contains__(user.branch.id):
                user_list_id.append(user.OneSignal_id)

        SendNotification_to_userIDs(user_list_id,
                                    "ZTY0ZTNmNjMtM2MwYS00ZTQ0LWJmODUtZGIwZTYyMWNjM2Ew",
                                    "morning time is near to be over",
                                    "Morning notification",
                                    "b51bb857-6a46-4119-a88c-ff6dac3ed079"
                                    )






# send notification to all the users who their time for morning questions are over
def morning_over_notif():
    # morning section
    if datetime.time.hour == models.WorkingHours.objects.all()[0]:
        rest_list_id = set()
        for ans in models.Answers.objects.all():
            if datetime.date.today() == ans.date and ans.Q_id.section == "morning":
                pass
            else:
                rest_list_id.add(ans.branch.id)

        print(rest_list_id)
        user_list_id = list()
        for user in models.UserProfile.objects.all():
            if rest_list_id.__contains__(user.branch.id):
                user_list_id.append(user.OneSignal_id)
        '''
         one signal part :
        import requests
        import json

        header = {"Content-Type": "application/json; charset=utf-8",
                  "Authorization": "Basic ZTY0ZTNmNjMtM2MwYS00ZTQ0LWJmODUtZGIwZTYyMWNjM2Ew"}

        payload = {"app_id": "b51bb857-6a46-4119-a88c-ff6dac3ed079",
                   "include_player_ids": user_list_id,
                   "contents": {"en": "Morning Section is over "}}

        req = requests.post("https://onesignal.com/api/v1/notifications", headers=header, data=json.dumps(payload))

        print(req.status_code, req.reason)
        '''
        SendNotification_to_userIDs(user_list_id,
                                    "ZTY0ZTNmNjMtM2MwYS00ZTQ0LWJmODUtZGIwZTYyMWNjM2Ew",
                                    "morning time over",
                                    "Morning notification",
                                    "b51bb857-6a46-4119-a88c-ff6dac3ed079"
                                    )






# send notification to all the users who their time for during the day questions are about to be over
def during_near_over_notif():
    # during section
    if datetime.time.hour == models.NearHours.objects.all()[0].during_hour and datetime.time.minute == models.NearHours.objects.all()[0].during_minute:
        rest_list_id = set()
        for ans in models.Answers.objects.all():
            if datetime.date.today() == ans.date and ans.Q_id.section == "morning":
                pass
            else:
                rest_list_id.add(ans.branch.id)

        print(rest_list_id)
        user_list_id = list()
        for user in models.UserProfile.objects.all():
            if rest_list_id.__contains__(user.branch.id):
                user_list_id.append(user.OneSignal_id)
        SendNotification_to_userIDs(user_list_id,
                                    "ZTY0ZTNmNjMtM2MwYS00ZTQ0LWJmODUtZGIwZTYyMWNjM2Ew",
                                    "During time is near to be over",
                                    "During notification",
                                    "b51bb857-6a46-4119-a88c-ff6dac3ed079"
                                    )





# send notification to all the users who their time for during the day questions are over
def during_over_notif():
    # during section
    if datetime.time.hour == models.WorkingHours.objects.all()[0].during_end:
        rest_list_id = set()
        for ans in models.Answers.objects.all():
            if datetime.date.today() == ans.date and ans.Q_id.section == "morning":
                pass
            else:
                rest_list_id.add(ans.branch.id)

        print(rest_list_id)
        user_list_id = list()
        for user in models.UserProfile.objects.all():
            if rest_list_id.__contains__(user.branch.id):
                user_list_id.append(user.OneSignal_id)
        SendNotification_to_userIDs(user_list_id,
                                    "ZTY0ZTNmNjMtM2MwYS00ZTQ0LWJmODUtZGIwZTYyMWNjM2Ew",
                                    "During time is over",
                                    "During notification",
                                    "b51bb857-6a46-4119-a88c-ff6dac3ed079"
                                    )





# send notification to all the users who their time for closing questions are about to be over
def closing_near_over_notif():
    # closing section
    if datetime.time.hour == models.NearHours.objects.all()[0].closing_hour and datetime.time.minute == models.NearHours.objects.all()[0].closing_minute:
        rest_list_id = set()
        for ans in models.Answers.objects.all():
            if datetime.date.today() == ans.date and ans.Q_id.section == "morning":
                pass
            else:
                rest_list_id.add(ans.branch.id)

        print(rest_list_id)
        user_list_id = list()
        for user in models.UserProfile.objects.all():
            if rest_list_id.__contains__(user.branch.id):
                user_list_id.append(user.OneSignal_id)
        SendNotification_to_userIDs(user_list_id,
                                    "ZTY0ZTNmNjMtM2MwYS00ZTQ0LWJmODUtZGIwZTYyMWNjM2Ew",
                                    "Closing time is near to be over",
                                    "Closing notification",
                                    "b51bb857-6a46-4119-a88c-ff6dac3ed079"
                                    )




# send notification to all the users who their time for closing questions are over
def closing_over_notif():
    # closing section
    if datetime.time.hour == models.WorkingHours.objects.all()[0].closing_end:
        rest_list_id = set()
        for ans in models.Answers.objects.all():
            if datetime.date.today() == ans.date and ans.Q_id.section == "morning":
                pass
            else:
                rest_list_id.add(ans.branch.id)

        print(rest_list_id)
        user_list_id = list()
        for user in models.UserProfile.objects.all():
            if rest_list_id.__contains__(user.branch.id):
                user_list_id.append(user.OneSignal_id)
        SendNotification_to_userIDs(user_list_id,
                                    "ZTY0ZTNmNjMtM2MwYS00ZTQ0LWJmODUtZGIwZTYyMWNjM2Ew",
                                    "Closing time is over",
                                    "Closing notification",
                                    "b51bb857-6a46-4119-a88c-ff6dac3ed079"
                                    )


def to_all():
    SendNotificationToAll("ZTY0ZTNmNjMtM2MwYS00ZTQ0LWJmODUtZGIwZTYyMWNjM2Ew",
                          "did you know your notification system works ?","Imen Food notification", "b51bb857-6a46-4119-a88c-ff6dac3ed079")
    for user in models.UserProfile.objects.all():
        notif = models.Notifications(content="did you know your notification system works ?",
                                 title="Imen Food notification",
                                 is_all=True,
                                user_id=user
                                 )
        notif.save()





# use the job scheduler to start them an run each checking code on a specified time
scheduler = BlockingScheduler()
scheduler.add_job(morning_near_over_notif, 'interval', minutes=1)
scheduler.add_job(morning_over_notif, 'interval', minutes=1)
scheduler.add_job(during_near_over_notif, 'interval', minutes=1)
scheduler.add_job(during_over_notif, 'interval', minutes=1)
scheduler.add_job(closing_near_over_notif, 'interval', minutes=1)
scheduler.add_job(closing_over_notif, 'interval', minutes=1)
scheduler.add_job(to_all, 'interval', seconds=15)


scheduler.start()