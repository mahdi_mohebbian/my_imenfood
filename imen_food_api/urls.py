"""imen_food_api URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from imen_food import views
from rest_framework.routers import SimpleRouter
from django.conf.urls.static import static
from . import settings

router = SimpleRouter()

router.register('^branch_abstract',viewset=views.BranchViewSet_abstract)
router.register('^branch_detail',viewset=views.BranchViewSet_detail)

router.register('^provider_product',viewset=views.Product_ProviderViewSet)
router.register('^user',viewset=views.UserProfileViewSet)

router.register('^equip',viewset=views.EquipmentViewSet)
router.register('^provider',viewset=views.ProviderViewSet)


urlpatterns = [

    url(r'^admin/', admin.site.urls),
    url(r'^api/v1/rest_equip/', views.RestaurantsEquipmentAPIView.as_view()),
    url(r'^api/v1/questions/', views.QuestionAPIView.as_view()),
    url(r'^api/v1/answers/', views.AnswerAPIView.as_view()),
    url(r'^api/v1/pending/', views.AdminPendingAPIView.as_view()),
    url(r'^api/v1/my_user/', views.MyUserProfileAPIVIew.as_view()),
    url(r'^api/v1/question_feed/', views.QuestionFeedAPIVIew.as_view()),
    url(r'^api/v1/user_branch_abstract/', views.BranchAPIView_abstract_user.as_view()),
    url(r'^api/v1/user_branch_detail/', views.BranchAPIView_detail_user.as_view()),
    url(r'^api/v1/login/', views.LoginAPIView.as_view()),
    url(r'^api/v1/faq/', views.FAQAPIView.as_view()),
    url(r'^api/v1/receipt/', views.ReceiptSendAPIVIew.as_view()),
    url(r'^api/v1/products/', views.Provider_productResponse.as_view()),
    url(r'^api/v1/hours/', views.WorkingHourPIView.as_view()),
    url(r'^api/v1/upload/', views.ImageuploadAPIView.as_view()),
    url(r'^api/v1/player_id/', views.PlayerIDAPIView.as_view()),
    url(r'^api/v1/notifications/', views.NotificationsAPIView.as_view()),
    url(r'^api/v1/links/', views.Support_and_Rules_LinkAPIView.as_view()),


# restaurant panel APIs


    url(r'^api/v1/login_admin/', views.Login_branch_admin_APIView.as_view()),

    url(r'^api/v1/notif_to_all/', views.NotificationSendToAll.as_view()),

    url(r'^api/v1/reinvestigate/', views.ReInvestigateAPIView.as_view()),

    url(r'^api/v1/register_new_user/', views.RegisterAPIView.as_view()),

    url(r'^api/v1/factor_admin/', views.FactorAdminPanelPopulationAPIView.as_view()),

    url(r'^api/v1/users/(?P<pk>[0-9]+)/$', views.UserProfileDetail_APIView.as_view()),

    url(r'^api/v1/rest_equip_detail/(?P<pk>[0-9]+)/$', views.RestaurantEquipmentDetail_put_deleteAPIView.as_view()),

    url(r'^api/v1/links/', views.Support_and_Rules_LinkAPIView.as_view()),

    url(r'^api/v1/my_branch_users/', views.MyBranchUsers_APIView.as_view()),

    url(r'^api/v1/logout/', views.LogoutAPIVIew.as_view()),

    url(r'^api/v1/activities/', views.ActivitiesAPIView.as_view()),

    url(r'^api/v1/restaurant_manager_register/', views.register_restaurant_manager_APIView.as_view()),



    url(r'^api/v1/', include(router.urls)),




]
urlpatterns += static(settings.MEDIA_URL, document_root = settings.MEDIA_ROOT)

