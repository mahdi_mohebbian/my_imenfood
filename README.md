

## Repository information

* this repository is used for the development of "Imen Food" application server-side. this server has been implemented using django rest framework.
* Version 1.0
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

## Installation

* The Django Rest Framework is based on python language so a python distribution is needed.
* you can download "anaconda python distribution 3.6" [from here](https://www.anaconda.com/download/)
* next, you should install django rest framework and django.
* for that matter first an environment variable of your current installed python is required, 
and django library needs to be installed using pip.
A narrow instruction for installation of both discussed matters is available [here](https://github.com/BurntSushi/nfldb/wiki/Python-&-pip-Windows-installation).
* After installation of pip, you can install django and django rest framework using pip.
* django installation : 
* ```
pip install django 
``` 

* django Rest FrameWork installation : 
* ```
pip install djangorestframework 
``` 

* After the installations you can start server by the following steps:
	* locate the root folder of project using ```cd```
	* example : ```cd .../python projects/imen_food_api```
	* ```python manage.py makemigrations```
	* ```python manage.py migrate```
	* start up server using the following command
	* ```python manage.py runserver [ip address+port]```
	* example : ```python manage.py runserver 192.168.1.80:8000```
	
* To use the Code Scheduler in order to run scripts of this server in their certain times you should use the "APScheduler" which would be downloaded using :
* ``` pip install apscheduler``` and there is a full documentation on it's usage in [here](http://apscheduler.readthedocs.io/en/latest/userguide.html).


## DataBase implementation

* Download and install the PostgreSQL for your windows version from [here](https://www.enterprisedb.com).
* To create a working connection between Django and postgreSQL you need a python package called "psycopg2" :
* Install : ``` pip install psycopg2 ```
* After the installation you can use "pgAdmin" to create a new database for the "imen food" project.
* ATTENTION (the information below must be accurate in the creation of the new database in the pdAdmin):
	* 'NAME': 'imen_food'
	* 'USER': 'bvar' (if needed, you should create bvar user)
	* 'PASSWORD': 'bvAaswq123r'
	* 'HOST': 'localhost'
	* 'PORT': '5432'
	
## Documentation

* In order to auto generate a document for this project, i used the "PYCCO" package. to receive this package use ```pip install``` command :
* ```pip install pycco```
* this package can auto generate a document for each python file. a sample document generation for "models.py" would be :
	* ```pycco models.py```





##License

* Developed in BVAR group co.

##Author
* Server Developer email-address :
* mahdi.mohebbian@yahoo.com